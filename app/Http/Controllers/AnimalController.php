<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Http;
use Inertia\Inertia;

class AnimalController extends Controller
{
    public function index()
    {
        $responseData = Http::get('https://hajusrakendus.ta22tiitma.itmajakas.ee/artwork')->json();

        return Inertia::render('Animals', ['animals' => $responseData]);
    }
}
