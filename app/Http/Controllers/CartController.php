<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Inertia\Inertia;

class CartController extends Controller
{
    public function showCart()
    {
        $cart = session()->get('cart', []);

        $totalPrice = collect($cart)->sum(function ($product) {
            return $product['price'] * $product['quantity'];
        });

        return Inertia::render('Cart', [
            'cart' => $cart,
            'totalPrice' => $totalPrice,
        ]);
    }

    public function addToCart(Request $request, Product $product)
    {
        $cart = session()->get('cart', []);

        if (isset($cart[$product->id])) {
            $cart[$product->id]['quantity'] += $request->input('quantity', 1);
        } else {
            $cart[$product->id] = [
                "title" => $product->title,
                "image" => $product->image,
                "price" => $product->price,
                "animal_type" => $product->animal_type,
                "quantity" => $request->quantity,
            ];
        }

        session()->put('cart', $cart);

        return redirect()->back()->with('success', 'Product added to cart successfully!');
    }

        public function updateCart(Request $request, Product $product)
    {
        $quantity = $request->input('quantity');

        $cart = session()->get('cart', []);

        if (isset($cart[$product->id])) {
            $cart[$product->id]['quantity'] = $quantity;
        }

        session()->put('cart', $cart);

        return redirect()->back()->with('success', 'Cart updated successfully!');
    }

        public function deleteFromCart(Product $product)
    {
        session()->forget('cart.' . $product->id);
    }

    public function deleteSession(Request $request)
    {
        $user = Auth::user();
        $request->session()->flush();
        Auth::login($user);
    }
}
