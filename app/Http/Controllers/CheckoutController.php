<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Inertia\Inertia;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Session;

class CheckoutController extends Controller
{

    public function index()
    {
        return Inertia::render('Checkout');
    }

    public function checkout()
    {

        \Stripe\Stripe::setApiKey(Config::get('services.stripe.secret'));

        $cart = Session::get('cart', []);
        $lineItems = [];

        foreach ($cart as $cartItem) {
            $lineItems[] = [
                'price_data' => [
                    'currency' => 'EUR',
                    'unit_amount' => $cartItem['price'] * 100,
                    'product_data' => [
                        'name' => $cartItem['title'],
                    ]
                ],
                'quantity' => $cartItem['quantity'],
            ];
        }

        $checkout_session = \Stripe\Checkout\Session::create([
            'line_items' => $lineItems,
            'mode' => 'payment',
            'success_url' => route('checkout.success'),
            'cancel_url' => route('checkout.cancel'),
        ]);

        return Inertia::location($checkout_session->url);
    }

    public function success()
    {
        Session::forget('cart');
        return Inertia::render('Success');
    }

    public function cancel()
    {
        return Inertia::render('Cancel');
    }
}
