<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Inertia\Inertia;

class DashController extends Controller
{
    public function index()
    {
        return Inertia::render('Dashboard', [
            'products' => Product::all()
        ]);
    }
}
