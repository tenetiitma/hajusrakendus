<?php

namespace App\Http\Controllers;

use App\Models\Marker;
use Illuminate\Http\Request;
use Inertia\Inertia;

class MarkerController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $markers = Marker::all();

        return Inertia::render(
            'Map',
            [
                'markers' => $markers
            ]
        );
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return Inertia::render(
            'Markers/Create'
        );
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        Marker::create(
            $request->validate([
                'name' => 'required|string|max:255',
                'desc' => 'required|string',
                'lat' => 'required|numeric|between:-90,90',
                'lng' => 'required|numeric|between:-180, 180',
            ])
        );

        return redirect()->back()->with('message', 'Marker added successfully!');
    }
    /**
     * Display the specified resource.
     */
    public function show(Marker $marker)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Marker $marker)
    {
        return Inertia::render(
            'Markers/Edit',
            [
                'marker' => $marker
            ]
        );
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Marker $marker)
    {

        $marker->update(
            $request->validate([
                'name' => 'required|string|max:255',
                'desc' => 'required|string',
                'lat' => 'required',
                'lng' => 'required',
            ])
        );

        return redirect()->to('/dashboard')->with('message', 'Marker updated successfully!');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Marker $marker)
    {
        $marker->delete();

        return redirect()->back()->with('message', 'Marker deleted!');
    }
}
