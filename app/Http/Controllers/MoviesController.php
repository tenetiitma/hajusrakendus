<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Http;
use Inertia\Inertia;

class MoviesController extends Controller

{
    public function index()
    {
    $responseData = Http::get('https://hajus.ta19heinsoo.itmajakas.ee/api/movies');
    $movies = $responseData->json()['data'];

    return Inertia::render('Movies', compact('movies'));
    }
}
