<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Inertia\Inertia;
use App\Models\Product;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $products = Product::all();

        return Inertia::render('Product/Index', [
            'products' => $products
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return Inertia::render('Product/Create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        // nüüd ta on muutuja
        $product = Product::create(
            $request->validate([
                'title' => 'required|string|max:255',
                'description' => 'required|string',
                'price' => 'required|numeric|min:0',
                'animal_type' => 'required|string|max:255',
                'file' => 'required'
            ])
        );

        $product->addMediaFromRequest('file')->toMediaCollection('images');

        return redirect()->route('products.index');
    }

    /**
     * Display the specified resource.
     */
    public function show()
    {
        return Inertia::render('Shop', [
            'products' => Product::all()
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Product $product)
    {
        return Inertia::render(
            'Product/Update',
            [
                'product' => $product,
            ]
        );
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Product $product)
    {

        $product->update(
            $request->validate([
                'title' => 'string|max:255',
                'description' => 'string',
                'price' => 'numeric|min:0',
                'animal_type' => 'string|max:255',
                'file' => '',
            ])
        );

        return redirect()->route('products.index');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Product $product)
    {
        $product->delete();

        return redirect()->back()->with('message', 'Product deleted!');
    }
}
