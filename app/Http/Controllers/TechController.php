<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Inertia\Inertia;

class TechController extends Controller
{
    public function index()
    {
        $responseData = Http::get('https://hajusrakendus.ta22korva.itmajakas.ee/api/shopapis')->json();

        return Inertia::render('Tech', ['techs' => $responseData]);
    }
}
