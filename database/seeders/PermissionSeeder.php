<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $role = Role::create(['name' => 'admin']);

        $user = User::firstOrCreate([
            'email' => 'tenetiitma@gmail.com',
        ], [
            'name' => 'admin',
            'password' => bcrypt('admin'),
        ]);

        $user->assignRole($role);
    }
}
