<?php

use App\Http\Controllers\AnimalController;
use App\Http\Controllers\CartController;
use App\Http\Controllers\MoviesController;
use App\Http\Controllers\PostController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\MarkerController;
use App\Http\Controllers\CommentController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\CheckoutController;
use App\Http\Controllers\DashController;
use App\Http\Controllers\OtherController;
use App\Http\Controllers\ShowApiController;
use App\Http\Controllers\TechController;
use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return Inertia::render('Welcome', [
        'canLogin' => Route::has('login'),
        'canRegister' => Route::has('register'),
        'laravelVersion' => Application::VERSION,
        'phpVersion' => PHP_VERSION,
    ]);
});

// Route::get('/dashboard', function () {
//     return Inertia::render('Dashboard');
// })->middleware(['auth', 'verified'])->name('dashboard');

Route::middleware('auth')->group(function () {
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
    Route::get('/shop', [ProductController::class, 'show'])->name('shop.show');
    Route::get('/movies', [MoviesController::class, 'index'])->name('movies.index');
    Route::get('/other', [OtherController::class, 'index'])->name('other.index');
    Route::get('/tech', [TechController::class, 'index'])->name('tech.index');
    Route::get('/animals', [AnimalController::class, 'index'])->name('animals.index');
    Route::get('/dashboard', [DashController::class, 'index'])->name('dash.index');
});

Route::get('/artwork', [ShowApiController::class, 'index']);

Route::controller(MarkerController::class)->middleware(['auth', 'verified'])->name('marker.')->group(function () {
    Route::get('/map', 'index')->name('index');
    Route::post('/map', 'store')->name('store');
    Route::put('/map/{marker}', 'update')->name('update');
    Route::delete('/map/{marker}', 'destroy')->name('destroy');
});

Route::controller(PostController::class)->middleware(['auth', 'verified'])->name('posts.')->group(function () {
    Route::get('/blog', 'index')->name('index');
    Route::post('/blog', 'store')->name('store');
    Route::put('/blog/{post}', 'update')->name('update');
    Route::delete('/blog/{post}', 'destroy')->name('destroy');
});

Route::controller(CommentController::class)->middleware(['auth', 'verified'])->name('comments.')->group(function () {
    Route::post('/comment', 'store')->name('store');
    Route::delete('/comment/{comment}', 'destroy')->name('destroy');
});

Route::controller(ProductController::class)->middleware(['auth', 'verified'])->name('products.')->group(function () {
    Route::get('/product', 'index')->name('index');
    Route::post('/product/create', 'store')->name('store');
    Route::get('/product/create', 'create');
    Route::get('/product/edit/{product}', 'edit')->name('edit');
    Route::put('/product/edit/{product}', 'update')->name('update');
    Route::delete('/product/{product}', 'destroy')->name('destroy');
});

Route::controller(CartController::class)->middleware(['auth', 'verified'])->name('cart.')->group(function () {
    Route::get('/cart', 'showCart')->name('cart');
    Route::post('/add-to-cart/{product}', 'addToCart')->name('addToCart');
    Route::patch('/update-cart/{product}', 'updateCart')->name('updateCart');
    Route::delete('/cart/{product}', 'deleteFromCart')->name('delete');
    Route::get('/delete-session', 'deleteSession')->name('deleteSession');
});

Route::controller(CheckoutController::class)->middleware(['auth', 'verified'])->name('checkout.')->group(function () {
    Route::get('/checkout', 'index')->name('index');
    Route::post('/checkout/sessions', 'checkout')->name('checkout');
    Route::get('/success', 'success')->name('success');
    Route::get('/cancel', 'cancel')->name('cancel');
});


require __DIR__ . '/auth.php';
